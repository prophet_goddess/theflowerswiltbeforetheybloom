﻿using TheFlowersWiltBeforeTheyBloom.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheFlowersWiltBeforeTheyBloom.Containers;

namespace TheFlowersWiltBeforeTheyBloom
{

    public static class Utility
    {
        public static int enemySpriteWidth = 96;

        public static void DrawBox(Rectangle rectangle, Color fillColor, Color borderColor, int borderWidth = 4, float fillPercent = 1f)
        {
            //draw the background
            Game.spriteBatch.Draw(Game.white16, new Rectangle(rectangle.X, rectangle.Y, (int)(rectangle.Width * fillPercent), rectangle.Height), fillColor);

            //draw the top border
            Game.spriteBatch.Draw(Game.white16, destinationRectangle:
                                        new Rectangle(rectangle.X, rectangle.Y, rectangle.Width, borderWidth),
                                        color: borderColor);

            //draw the bottom border
            Game.spriteBatch.Draw(Game.white16, destinationRectangle:
                                        new Rectangle(rectangle.X, rectangle.Y + rectangle.Height, rectangle.Width, borderWidth),
                                        color: borderColor);

            //draw the left border
            Game.spriteBatch.Draw(Game.white16, destinationRectangle:
                                        new Rectangle(rectangle.X, rectangle.Y, borderWidth, rectangle.Height),
                                        color: borderColor);

            //draw the right border
            Game.spriteBatch.Draw(Game.white16, destinationRectangle:
                                      new Rectangle(rectangle.X + (rectangle.Width - borderWidth), rectangle.Y,
                                      borderWidth, rectangle.Height),
                                      color: borderColor);
        }


        public static int RoundToMultipleOf(float n, int mult)
        {
            return (int)(Math.Round(n / mult) * mult);
        }


        public static Texture2D GetEnemySprite(float hue, int borderWidth = 4)
        {
            var texture = new Texture2D(Game.graphics.GraphicsDevice, enemySpriteWidth, enemySpriteWidth);
            var pixelData = new Color[texture.Width * texture.Height];

            var rects = new Rectangle[RNG.GetIntInclusive(16, 64)];
            for (var i = 0; i < rects.Length; i++)
            {
                var width = RoundToMultipleOf(RNG.GetInt(0, texture.Width / 2), 4);
                var height = RoundToMultipleOf(RNG.GetInt(0, texture.Height / 2), 4);

                var x = RoundToMultipleOf(RNG.GetInt(borderWidth, texture.Width - width - borderWidth - 1), 4);
                var y = RoundToMultipleOf(RNG.GetInt(borderWidth, texture.Height - height - borderWidth - 1), 4);

                rects[i] = new Rectangle(x, y, width, height);
            }


            float saturation = RNG.GetFloat(0.5f, 1.0f);
            float value = RNG.GetFloat(0.5f, 1.0f);

            foreach (Rectangle rect in rects)
            {

                var color = RNG.GetFloat() >= 0.33f ? ColorFromHSV(hue, saturation, value) : Color.Transparent;
                for (int x = rect.X; x < rect.X + rect.Width; x++)
                {
                    for (int y = rect.Y; y < rect.Y + rect.Height; y++)
                    {
                        pixelData[texture.Width * y + x] = color;
                    }
                }
            }


            var finalData = new Color[texture.Width * texture.Height];

            for (int x = 1; x < texture.Width - 1; x++)
            {
                for (int y = 1; y < texture.Height - 1; y++)
                {
                    if (pixelData[texture.Width * y + x].A == 0f)
                    {
                        for (int dist = 1; dist <= borderWidth; dist++)
                        {
                            var neighbors = new[] {
                                y + dist < texture.Height ? pixelData[texture.Width * (y + dist) + x] : Color.Transparent,
                                y - dist >= 0 ? pixelData[texture.Width * (y - dist) + x] : Color.Transparent,
                                x - dist >= 0 ? pixelData[texture.Width * y + (x - dist)] : Color.Transparent,
                                x + dist < texture.Width ? pixelData[texture.Width * y + (x + dist)] : Color.Transparent,
                                y - dist >= 0 && x + dist < texture.Width ? pixelData[texture.Width * (y - dist) + (x + dist)] : Color.Transparent,
                                y - dist >= 0 && x - dist >= 0 ? pixelData[texture.Width * (y - dist) + (x - dist)] : Color.Transparent,
                                y + dist < texture.Height && x - dist >= 0 ? pixelData[texture.Width * (y + dist) + (x - dist)] : Color.Transparent,
                                y + dist < texture.Height && x + dist < texture.Width ? pixelData[texture.Width * (y + dist) + (x + dist)] : Color.Transparent,
                            };

                            if (neighbors.Any((Color c) => c.A > 0f))
                            {
                                finalData[texture.Width * y + x] = Colors.outline;
                            }
                        }
                    }
                    else
                    {
                        finalData[texture.Width * y + x] = pixelData[texture.Width * y + x];
                    }

                }
            }

            texture.SetData(finalData);

            return texture;
        }

        public static Color ColorFromHSV(float hue, float saturation, float value)
        {
            int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
            double f = hue / 60 - Math.Floor(hue / 60);

            value = value * 255;
            int v = Convert.ToInt32(value);
            int p = Convert.ToInt32(value * (1 - saturation));
            int q = Convert.ToInt32(value * (1 - f * saturation));
            int t = Convert.ToInt32(value * (1 - (1 - f) * saturation));

            if (hi == 0)
                return new Color(v, t, p);
            else if (hi == 1)
                return new Color(q, v, p);
            else if (hi == 2)
                return new Color(p, v, t);
            else if (hi == 3)
                return new Color(p, q, v);
            else if (hi == 4)
                return new Color(t, p, v);
            else
                return new Color(v, p, q);
        }

        public static int GetMaxHP(int stamina)
        {
            return stamina * 15;
        }

        public static int GetMaxSP(int special)
        {
            return special * 5;
        }

        public static int GetDamage(int strength)
        {
            int margin = (int)Math.Floor(strength * 0.25f);
            return -RNG.GetInt(strength - margin, strength + margin);
        }

        public static string GetLineWithId(string id)
        {
            return Game.encounterText.GetElementById(id).ChildNodes.GetRandomNode().InnerText.Trim();
        }

    }
}
