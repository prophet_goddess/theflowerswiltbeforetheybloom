﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using Microsoft.Xna.Framework;
using System.Text;
using SpriteFontPlus;

namespace TheFlowersWiltBeforeTheyBloom.Renderers
{
    public class ChoiceRenderer : GeneralRenderer
    {
        public override void Render()
        {
            if(!SomeComponent<ChoicesComponent>() || !SomeComponent<BackgroundComponent>() || !SomeComponent<LogComponent>())
            {
                return;
            }

            var choices = ReadComponent<ChoicesComponent>();

            var choicesText = new StringBuilder();
            var i = 0;
            foreach(var choice in choices.choices)
            {
                if(i == choices.selectedIndex)
                {
                    choicesText.Append("» ");
                }
                else
                {
                    choicesText.Append("· ");
                }

                choicesText.Append(choice);
                choicesText.Append(System.Environment.NewLine);

                i++;
            }

            var choicesString = choicesText.ToString().Trim();
            var choicesSize = Game.bodyFont.MeasureString(choicesString);
            var width = choicesSize.X + Game.boxTextMargin*2f + Game.boxBorderSize;
            var height = choicesSize.Y + Game.boxTextMargin;

            var log = ReadComponent<LogComponent>();
            var logHeight = Game.windowHeight * log.relativeHeight;

            var background = ReadComponent<BackgroundComponent>();
            var backgroundHeight = Game.windowHeight - logHeight;
            var backgroundWidth = backgroundHeight * ((float)background.texture.Width / background.texture.Height);

            var rectangle = new Rectangle((int)(Game.windowWidth - backgroundWidth/2 - width/2), (int)(backgroundHeight/2 - height/2), (int)width, (int)height);

            Utility.DrawBox(rectangle, Color.Black, Color.White);
            Game.spriteBatch.DrawString(Game.bodyFont, choicesString, new Vector2(rectangle.X + Game.boxTextMargin, rectangle.Y + Game.boxTextMargin), Color.White);

        }
    }
}
