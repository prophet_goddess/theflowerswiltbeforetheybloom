﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using Microsoft.Xna.Framework;
using SpriteFontPlus;

namespace TheFlowersWiltBeforeTheyBloom.Renderers
{
    public class SidebarRenderer : GeneralRenderer
    {
        public override void Render()
        {
            if (!SomeComponent<BackgroundComponent>() || !SomeComponent<LogComponent>())
            {
                return;
            }

            var log = ReadComponent<LogComponent>();

            var logHeight = Game.windowHeight * log.relativeHeight;

            var background = ReadComponent<BackgroundComponent>();
            var backgroundHeight = Game.windowHeight - logHeight;
            var backgroundWidth = backgroundHeight * ((float)background.texture.Width / background.texture.Height);

            var rectangle = new Rectangle(0, 0, Game.windowWidth - (int)backgroundWidth + Game.boxBorderSize, Game.windowHeight - (int)logHeight);
            Utility.DrawBox(rectangle, Color.Black, Color.White);

            if (!SomeComponent<StatusComponent>())
            {
                return;
            }

            var statusComponent = ReadComponent<StatusComponent>();

            var statusText = string.Format("{0}\n----\nHP: {1}\nSP: {2}", "Vigil\nYour Bedroom", statusComponent.hitPoints, statusComponent.specialPoints);

            var textPosition = new Vector2(rectangle.X, rectangle.Y);
            textPosition.X += Game.boxTextMargin + Game.boxBorderSize;
            textPosition.Y += Game.boxTextMargin + Game.boxBorderSize;

            Game.spriteBatch.DrawString(Game.bodyFont, statusText, textPosition, Color.White);

        }
    }
}
