﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace TheFlowersWiltBeforeTheyBloom.Renderers
{
    public class BackgroundRenderer : GeneralRenderer
    {
        public override void Render()
        {
            if (!SomeComponent<BackgroundComponent>() || !SomeComponent<LogComponent>())
            {
                return;
            }

            var backgroundComponent = ReadComponent<BackgroundComponent>();
            var logComponent = ReadComponent<LogComponent>();
            var logHeight = Game.windowHeight * logComponent.relativeHeight;
            var height = Game.windowHeight - logHeight;
            var width = height * ((float)backgroundComponent.texture.Width / backgroundComponent.texture.Height);

            var targetRectangle = new Rectangle(Game.windowWidth - (int)width, 0, (int)width, (int)height);


            Game.spriteBatch.End();

            Game.spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp,
                DepthStencilState.None, RasterizerState.CullNone, backgroundComponent.effect);


            Game.spriteBatch.Draw(backgroundComponent.texture, targetRectangle, Color.White);
            Game.spriteBatch.End();

            Game.spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp,
                 DepthStencilState.None, RasterizerState.CullNone);

            Utility.DrawBox(targetRectangle, Color.Transparent, Colors.white);


        }
    }
}
