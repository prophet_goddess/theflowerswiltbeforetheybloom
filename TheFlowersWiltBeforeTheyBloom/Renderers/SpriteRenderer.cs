﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using Microsoft.Xna.Framework;
using System;

namespace TheFlowersWiltBeforeTheyBloom.Renderers
{
    public class SpriteRenderer : GeneralRenderer
    {
        public override void Render()
        {
            foreach ((var sprite, var spriteEntity) in ReadComponentsIncludingEntity<SpriteComponent>())
            {
                var transform = GetComponent<TransformComponent>(spriteEntity);

                //Game.spriteBatch.Draw(texture: sprite.texture, position: transform.position + sprite.offset,
                //                      color: sprite.flicker ? Color.Transparent : sprite.color,
                //                      origin: new Vector2(sprite.texture.Width / 2f, sprite.texture.Height / 2f),
                //                      scale: sprite.scale);
            }
        }
    }
}
