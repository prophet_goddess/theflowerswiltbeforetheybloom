﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TheFlowersWiltBeforeTheyBloom.Renderers
{
    public class EnemyHealthRenderer : GeneralRenderer
    {
        public override void Render()
        {
            var statuses = ReadComponentsIncludingEntity<StatusComponent>();

            foreach((var status, var statusEntity) in statuses){
                if (HasComponent<EnemyComponent>(statusEntity))
                {
                    var info = GetComponent<EncounterInfoComponent>(statusEntity);

                    if (info.targeted)
                    {
                        var transform = GetComponent<TransformComponent>(statusEntity);
                        var sprite = GetComponent<SpriteComponent>(statusEntity);

                        Utility.DrawBox(new Rectangle((int)(transform.position.X - sprite.texture.Width / 2f),
                                                      (int)((transform.position.Y + sprite.texture.Height / 1.5f)),
                                                      sprite.texture.Width, (int)(sprite.texture.Height * 0.1f)),
                                                      Color.Red, Color.Transparent, 2, (float)status.hitPoints / (float)status.maxHitPoints);
                    }
                }
                
            }
        }
    }
}
