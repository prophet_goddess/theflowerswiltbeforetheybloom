﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Diagnostics;
using System.Linq;

namespace TheFlowersWiltBeforeTheyBloom.Renderers
{
    public class TargetSelectRenderer : GeneralRenderer
    {

        public override void Render()
        {
            var encounterInfos = ReadComponentsIncludingEntity<EncounterInfoComponent>();

            foreach ((var encounterInfo, var encounterInfoEntity) in encounterInfos)
            {

                if (HasComponent<SpriteComponent>(encounterInfoEntity))
                {
                    var sprite = GetComponent<SpriteComponent>(encounterInfoEntity);
                    var transform = GetComponent<TransformComponent>(encounterInfoEntity);
                    var info = GetComponent<EncounterInfoComponent>(encounterInfoEntity);

                    if (info.targeted)
                    {
                        double t = Game.gameTime.TotalGameTime.TotalSeconds;
                        //Game.spriteBatch.Draw(texture: Game.selectorSprite, position: transform.position - (Vector2.UnitY * (sprite.texture.Height / 1.5f + (float)Math.Sin(t) * 4f)),
                        //  color: Color.White,
                        //  origin: new Vector2(Game.selectorSprite.Width / 2f, Game.selectorSprite.Height / 2f));
                    }
                }

            }
        }
    }
}
