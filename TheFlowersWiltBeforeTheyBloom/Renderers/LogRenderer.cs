﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using Microsoft.Xna.Framework;
using SpriteFontPlus;
using Microsoft.Xna.Framework.Graphics;

namespace TheFlowersWiltBeforeTheyBloom.Renderers
{
    public class LogRenderer : GeneralRenderer
    {
        public override void Render()
        {
            if (!SomeComponent<LogComponent>())
            {
                return;
            }

            var log = ReadComponent<LogComponent>();
            var logHeight = (Game.windowHeight * log.relativeHeight);

            var rectangle = new Rectangle(0, Game.windowHeight - (int)logHeight, Game.windowWidth, (int)logHeight - Game.boxBorderSize/2);

            Utility.DrawBox(rectangle, Colors.black, Colors.white);

            var textPosition = new Vector2(rectangle.X, rectangle.Y);
            textPosition.X += Game.boxTextMargin + Game.boxBorderSize;
            textPosition.Y += Game.boxTextMargin + Game.boxBorderSize;

            Game.spriteBatch.DrawString(Game.bodyFont, log.displayMessage, textPosition, Color.White);
        }
    }
}
