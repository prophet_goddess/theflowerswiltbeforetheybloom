﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct UpdateSpriteMessage : IMessage
    {
        public Entity spriteEntity;
        public Vector2 scale;
        public bool flicker;
    }
}
