﻿using Encompass;

namespace TheFlowersWiltBeforeTheyBloom.Messages {
    public struct DestroyChoiceMessage : IMessage {
        public Entity entity;
    }
}
