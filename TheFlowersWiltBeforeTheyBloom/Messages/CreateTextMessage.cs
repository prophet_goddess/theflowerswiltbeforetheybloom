﻿using Encompass;
using Microsoft.Xna.Framework;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct CreateTextMessage : IMessage
    {
        public Entity entity;
        public string text;
        public Vector2 offset;
    }
}
