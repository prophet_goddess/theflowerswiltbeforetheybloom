﻿using Encompass;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct AddToCombatLogMessage : IMessage
    {
        public string message;
        public float characterDelay;
    }
}
