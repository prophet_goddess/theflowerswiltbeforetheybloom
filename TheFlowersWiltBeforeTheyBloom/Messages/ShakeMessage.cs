﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct ShakeMessage : IMessage
    {
        public Entity transformEntity;
        public float intensity;
        public float duration;
    }
}
