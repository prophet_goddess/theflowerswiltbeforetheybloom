﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct SpriteFlickerMessage : IMessage
    {
        public Entity spriteEntity;
        public float duration;
        public float speed;
    }
}
