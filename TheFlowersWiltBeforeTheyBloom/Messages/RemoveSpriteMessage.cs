﻿using Encompass;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct RemoveSpriteMessage : IMessage
    {
        public Entity entity;
    }
}
