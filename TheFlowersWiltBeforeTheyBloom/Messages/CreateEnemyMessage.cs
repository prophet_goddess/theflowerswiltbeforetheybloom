﻿using Encompass;
using Microsoft.Xna.Framework;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct CreateEnemyMessage : IMessage
    {
        public int index;
        public Vector2 position;
    }
}
