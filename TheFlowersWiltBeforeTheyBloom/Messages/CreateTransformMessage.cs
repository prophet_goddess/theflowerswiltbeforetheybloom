﻿using Encompass;
using Microsoft.Xna.Framework;

namespace TheFlowersWiltBeforeTheyBloom.Messages {
    public struct CreateTransformMessage : IMessage {
        public Entity entity;
        public Vector2 position;
    }
}
