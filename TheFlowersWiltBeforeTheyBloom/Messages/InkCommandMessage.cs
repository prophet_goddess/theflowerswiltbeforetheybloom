﻿using Encompass;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct InkCommandMessage : IMessage
    {
        public string command;
    }
}
