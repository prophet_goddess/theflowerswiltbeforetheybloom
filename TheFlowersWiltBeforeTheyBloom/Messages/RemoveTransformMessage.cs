﻿using Encompass;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct RemoveTransformMessage : IMessage
    {
        public Entity entity;
    }
}
