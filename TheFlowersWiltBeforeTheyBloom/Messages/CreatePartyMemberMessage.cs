﻿using Encompass;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct CreatePartyMemberMessage : IMessage
    {
        public string name;
    }
}
