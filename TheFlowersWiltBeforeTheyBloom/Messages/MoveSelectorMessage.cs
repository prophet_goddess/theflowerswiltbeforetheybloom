﻿using Encompass;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public enum Direction
    {
        Up, Down, Left, Right
    };

    public struct MoveSelectorMessage : IMessage
    {
        public Direction direction;
    }
}
