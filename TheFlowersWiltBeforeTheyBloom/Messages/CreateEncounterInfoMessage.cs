﻿using Encompass;

namespace TheFlowersWiltBeforeTheyBloom.Messages {
    public struct CreateEncounterInfoMessage : IMessage {
        public Entity entity;
    }
}
