﻿using Encompass;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct RemoveTextMessage : IMessage
    {
        public Entity entity;
    }
}
