﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct KillMessage : IMessage
    {
        public Entity statusEntity;
    }
}
