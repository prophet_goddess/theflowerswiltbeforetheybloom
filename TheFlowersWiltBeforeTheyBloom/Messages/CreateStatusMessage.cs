﻿using Encompass;

namespace TheFlowersWiltBeforeTheyBloom.Messages {
    public struct CreateStatusMessage : IMessage {
        public Entity entity;
        public string name;
        public int strength;
        public int stamina;
        public int speed;
        public int special;
    }
}
