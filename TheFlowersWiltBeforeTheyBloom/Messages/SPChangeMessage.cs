﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct SPChangeMessage : IMessage
    {
        public int amount;
        public string sourceName;
        public Entity targetEntity;
    }
}
