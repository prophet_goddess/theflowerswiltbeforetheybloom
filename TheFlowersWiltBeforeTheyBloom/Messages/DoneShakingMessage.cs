﻿using Encompass;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct DoneShakingMessage : IMessage
    {
        public Entity entity;
    }
}
