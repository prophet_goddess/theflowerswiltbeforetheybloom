﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct HPChangeMessage : IMessage
    {
        public int amount;
        public string sourceName;
        public Entity targetEntity;
    }
}
