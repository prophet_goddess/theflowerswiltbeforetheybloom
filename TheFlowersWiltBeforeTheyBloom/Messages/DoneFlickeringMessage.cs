﻿using Encompass;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct DoneFlickeringMessage : IMessage
    {
        public Entity entity;
    }
}
