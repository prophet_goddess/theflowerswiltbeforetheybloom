﻿using Encompass;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TheFlowersWiltBeforeTheyBloom.Messages {
    public struct CreateSpriteMessage : IMessage {
        public Entity entity;
        public Vector2 scale;
        public Texture2D texture;
        public Color color;
        public Vector2 offset;
    }
}
