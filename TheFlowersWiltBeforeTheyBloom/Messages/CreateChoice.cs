﻿using Encompass;

namespace TheFlowersWiltBeforeTheyBloom.Messages {
    public struct CreateChoiceMessage : IMessage {
        public string[] choices;
    }
}
