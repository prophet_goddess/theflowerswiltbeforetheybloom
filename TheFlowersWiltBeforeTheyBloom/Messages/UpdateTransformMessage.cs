﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using Microsoft.Xna.Framework;

namespace TheFlowersWiltBeforeTheyBloom.Messages
{
    public struct UpdateTransformMessage : IMessage
    {
        public Entity transformEntity;
        public Vector2 newPosition;
    }
}
