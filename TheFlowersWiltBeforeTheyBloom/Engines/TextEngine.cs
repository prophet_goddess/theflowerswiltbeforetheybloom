﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Reads(typeof(TextComponent))]
    [Writes(typeof(TextComponent))]
    [Receives(typeof(CreateTextMessage), typeof(RemoveTextMessage))]
    public class TextEngine : Engine
    {
        public override void Update(double dt) {

            var createTextMessages = ReadMessages<CreateTextMessage>();
            foreach (var createText in createTextMessages) {
                TextComponent textComponent;
                textComponent.text = createText.text;
                textComponent.offset = createText.offset;
                SetComponent(createText.entity, textComponent);
            }


            var removeTextMessages = ReadMessages<RemoveTextMessage>();
            foreach (var removeText in removeTextMessages) {
                RemoveComponent<TextComponent>(removeText.entity);
            }

        }
    }
}
