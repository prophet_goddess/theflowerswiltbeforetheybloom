﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Messages;
using System.Text.RegularExpressions;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Receives(typeof(InkCommandMessage))]
    [Sends(typeof(CreatePartyMemberMessage), typeof(CreateEncounterMessage))]
    public class InkCommandEngine : Engine
    {
        public override void Update(double dt)
        {
            var inkCommandMessages = ReadMessages<InkCommandMessage>();
            foreach (var inkCommand in inkCommandMessages)
            {
                if (inkCommand.command.Contains("addPartyMember"))
                {
                    var rx = new Regex(@""".*""");
                    var match = rx.Match(inkCommand.command);


                    CreatePartyMemberMessage createPartyMember;
                    createPartyMember.name = Regex.Replace(match.ToString(), @"""", "");
                    SendMessage(createPartyMember);
                }
                else if (inkCommand.command.Contains("startEncounter"))
                {
                    SendMessage(new CreateEncounterMessage());
                }

            }
        }
    }
}
