﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Reads(typeof(StatusComponent), typeof(EncounterComponent), typeof(PartyMemberComponent),
            typeof(EnemyComponent), typeof(EncounterInfoComponent), typeof(TransformComponent),
            typeof(LogComponent), typeof(ChoicesComponent), typeof(PartyComponent))]
    [Receives(typeof(ContinueMessage), typeof(MoveSelectorMessage), typeof(CreateEncounterInfoMessage))]
    [Sends(typeof(AddToCombatLogMessage), typeof(HPChangeMessage), typeof(CreateChoiceMessage))]
    [Writes(typeof(EncounterInfoComponent))]
    public class EncounterEngine : Engine
    {
        private int targetIndex = 0;

        private void ResetTurnStates(List<(EncounterInfoComponent, Entity)> infoComponents) {
            for (int i = 0; i < infoComponents.Count; i++) {
                var info = infoComponents[i];
                info.Item1.turnState = TurnState.NotTakenTurn;
                info.Item1.targeted = false;
                SetComponent(info.Item2, info.Item1);
            }
        }

        public override void Update(double dt) {

            var createEncounterInfoMessages = ReadMessages<CreateEncounterInfoMessage>();
            foreach (var message in createEncounterInfoMessages) {
                EncounterInfoComponent infoComponent;
                infoComponent.turnState = TurnState.NotTakenTurn;
                infoComponent.targeted = false;
                SetComponent(message.entity, infoComponent);
            }

            if (!SomeComponent<EncounterComponent>() || ReadComponent<LogComponent>().displaying) {
                return;
            }


            var infoComponents = ReadComponentsIncludingEntity<EncounterInfoComponent>()
                                .OrderByDescending(e => GetComponent<StatusComponent>(e.Item2).speed).ToList();


            if (!SomeComponent<EnemyComponent>()) {
                if (SomeMessage<ContinueMessage>()) {
                    ResetTurnStates(infoComponents);
                    Destroy(ReadComponentIncludingEntity<EncounterComponent>().Item2);
                    return;
                }
            }
            else if (!SomeComponent<PartyMemberComponent>()) {
                ResetTurnStates(infoComponents);
                Destroy(ReadComponentIncludingEntity<EncounterComponent>().Item2);
                return;
            }

            var enemies = ReadComponentsIncludingEntity<EnemyComponent>()
                          .OrderBy(e => GetComponent<TransformComponent>(e.Item2).position.X).ToList();

            if (infoComponents.Any(i => i.Item1.turnState == TurnState.IsTurn)) {
                (var isTurn, var isTurnEntity) = infoComponents.Where(i => i.Item1.turnState == TurnState.IsTurn).First();
                var isTurnStatus = GetComponent<StatusComponent>(isTurnEntity);

                if (HasComponent<EnemyComponent>(isTurnEntity)) {
                    HPChangeMessage hpChangeMessage;
                    hpChangeMessage.amount = Utility.GetDamage(isTurnStatus.strength);
                    hpChangeMessage.sourceName = isTurnStatus.name;
                    hpChangeMessage.targetEntity = ReadComponentsIncludingEntity<PartyMemberComponent>().ToArray().GetRandomItem().Item2;
                    SendMessage(hpChangeMessage);

                    isTurn.turnState = TurnState.TakenTurn;
                    SetComponent(isTurnEntity, isTurn);
                    return;
                }
                else if (HasComponent<PartyMemberComponent>(isTurnEntity)) {
                    (var party, var partyEntity) = ReadComponentIncludingEntity<PartyComponent>();

                    if (!SomeComponent<ChoicesComponent>()) {
                        CreateChoiceMessage createChoice;
                        createChoice.choices = new string[]{
                            Utility.GetLineWithId("attack"),
                            Utility.GetLineWithId("magic"),
                            Utility.GetLineWithId("item"),
                            Utility.GetLineWithId("run")
                        };
                        SendMessage(createChoice);
                    }
                    else{
                        (var choices, var choicesEntity) = ReadComponentIncludingEntity<ChoicesComponent>();
                        if (choices.selected) {
                            switch (choices.selectedIndex) {
                                case 0:
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    break;
                            }
                        }
                    }
                    return;
                }
            }

            if (infoComponents.All(i => i.Item1.turnState == TurnState.TakenTurn)) {
                ResetTurnStates(infoComponents);
                return;
            }

            var continueMessages = ReadMessages<ContinueMessage>();

            foreach (var continueMessage in continueMessages) {

                (var next, var nextEntity) = infoComponents.First(s => GetComponent<EncounterInfoComponent>(s.Item2).turnState == TurnState.NotTakenTurn);
                var nextStatus = GetComponent<StatusComponent>(nextEntity);

                next.turnState = TurnState.IsTurn;

                if (HasComponent<PartyMemberComponent>(nextEntity)) {

                    if (targetIndex < 0) {
                        targetIndex = enemies.Count - 1;
                    }
                    else if (targetIndex >= enemies.Count) {
                        targetIndex = 0;
                    }

                    AddToCombatLogMessage addToCombatLog;
                    addToCombatLog.message = Utility.GetLineWithId("playerTurn").Replace("%partyMember", nextStatus.name);
                    addToCombatLog.characterDelay = 0.025f;
                    SendMessage(addToCombatLog);
                }

                SetComponent(nextEntity, next);
            }

        }
    }
}
