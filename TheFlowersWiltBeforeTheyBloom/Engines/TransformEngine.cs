﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Reads(typeof(TransformComponent))]
    [Receives(typeof(UpdateTransformMessage), typeof(CreateTransformMessage), typeof(RemoveTransformMessage))]
    [Writes(typeof(TransformComponent))]
    public class TransformEngine : Engine
    {
        public override void Update(double dt)
        {
            var updates = new Dictionary<Entity, Vector2>();
            var createTransformMessages = ReadMessages<CreateTransformMessage>();
            foreach(var createTransformMessage in createTransformMessages) {
                TransformComponent transform;
                transform.position = createTransformMessage.position;
                SetComponent(createTransformMessage.entity, transform);
            }

            var removeTransformMessages = ReadMessages<RemoveTransformMessage>();
            foreach(var removeTransform in removeTransformMessages) {
                RemoveComponent<TransformComponent>(removeTransform.entity);
            }

            foreach(var updateTransformMessage in ReadMessages<UpdateTransformMessage>())
            {
                var transform = GetComponent<TransformComponent>(updateTransformMessage.transformEntity);
                if (!updates.ContainsKey(updateTransformMessage.transformEntity))
                {
                    updates.Add(updateTransformMessage.transformEntity, updateTransformMessage.newPosition - transform.position);
                }
                else
                {
                    updates[updateTransformMessage.transformEntity] += (updateTransformMessage.newPosition - transform.position);
                }

            }

            foreach(var update in updates)
            {
                var transform = GetComponent<TransformComponent>(update.Key);
                transform.position += update.Value;
                SetComponent(update.Key, transform);
            }
        }
    }
}
