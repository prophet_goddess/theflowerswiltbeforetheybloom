﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;
using Microsoft.Xna.Framework;
using System.Linq;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Reads(typeof(PartyMemberComponent), typeof(PartyComponent), typeof(BackgroundComponent), typeof(LogComponent))]
    [Writes(typeof(PartyMemberComponent))]
    [Sends(typeof(CreateEncounterInfoMessage), typeof(CreateStatusMessage), typeof(CreateTransformMessage), typeof(CreatePartyMessage))]
    [Receives(typeof(CreatePartyMemberMessage))]
    public class PartyMemberEngine : Engine
    {
        public override void Update(double dt)
        {
            var boxHeight = Game.windowHeight / 4;
            var boxWidth = boxHeight;
            var boxCenter = Game.windowWidth / 2 - boxWidth / 2;


            var createPartyMemberMessages = ReadMessages<CreatePartyMemberMessage>();
            foreach(var createPartyMember in createPartyMemberMessages)
            {
                if (!SomeComponent<PartyComponent>()) {
                    SendMessage(new CreatePartyMessage());
                }

                var partyMemberEntity = CreateEntity();

                var background = ReadComponent<BackgroundComponent>();
                var (log, logEntity) = ReadComponentIncludingEntity<LogComponent>();

                CreateEncounterInfoMessage createEncounterInfoMessage;
                createEncounterInfoMessage.entity = partyMemberEntity;
                SendMessage(createEncounterInfoMessage);

                CreateStatusMessage createStatus;
                createStatus.entity = partyMemberEntity;
                createStatus.name = createPartyMember.name;
                createStatus.strength = RNG.GetInt(15, 30);
                createStatus.stamina = RNG.GetInt(15, 30);
                createStatus.speed = RNG.GetInt(15, 30);
                createStatus.special = RNG.GetInt(15, 30);
                SendMessage(createStatus);

                CreateTransformMessage createTransform;
                createTransform.entity = partyMemberEntity;
                createTransform.position = new Vector2(0, 0);
                SendMessage(createTransform);

                SetComponent(partyMemberEntity, new PartyMemberComponent());
            }
        }
    }
}
