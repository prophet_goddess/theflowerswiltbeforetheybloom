﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using System.Text;

namespace TheFlowersWiltBeforeTheyBloom.Engines {

    [Receives(typeof(CreateChoiceMessage), typeof(ContinueMessage), typeof(MoveSelectorMessage))]
    [Reads(typeof(ChoicesComponent), typeof(BackgroundComponent))]
    [Writes(typeof(ChoicesComponent))]
    [Sends(typeof(CreateTransformMessage))]
    public class ChoiceEngine : Engine {
        public override void Update(double dt) {
            do {
                if (SomeComponent<ChoicesComponent>()) {
                    (var choices, var choicesEntity) = ReadComponentIncludingEntity<ChoicesComponent>();

                    if (choices.selected) {
                        break;
                    }

                    var continues = ReadMessages<ContinueMessage>();
                    foreach (var cont in continues) {
                        choices.selected = true;
                        SetComponent(choicesEntity, choices);
                        break;
                    }

                    var moveSelectorMessages = ReadMessages<MoveSelectorMessage>();
                    foreach (MoveSelectorMessage moveSelectorMessage in moveSelectorMessages) {

                        if (moveSelectorMessage.direction == Direction.Up && choices.selectedIndex > 0) {
                            choices.selectedIndex--;
                        }
                        else if (moveSelectorMessage.direction == Direction.Down && choices.selectedIndex < choices.choices.Length - 1) {
                            choices.selectedIndex++;
                        }
                    }

                    SetComponent(choicesEntity, choices);
                }
            } while (false);



            var createChoiceMessages = ReadMessages<CreateChoiceMessage>();
            foreach (var createChoice in createChoiceMessages) {
                var choiceEntity = CreateEntity();

                ChoicesComponent choicesComponent;
                choicesComponent.choices = createChoice.choices;
                choicesComponent.selectedIndex = 0;
                choicesComponent.selected = false;
                SetComponent(choiceEntity, choicesComponent);

            }
        }
    }
}
