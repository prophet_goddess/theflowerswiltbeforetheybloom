﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;
using System.Linq;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Reads(typeof(InkStoryComponent), typeof(PartyMemberComponent))]
    [Sends(typeof(ContinueMessage))]
    [Receives(typeof(InitializeInkMessage))]
    public class InitializeInkEngine : Engine
    {
        public override void Update(double dt)
        {
            var initializeInkMessages = ReadMessages<InitializeInkMessage>();


            foreach (var initializeInkMessage in initializeInkMessages)
            {
                ContinueMessage inkBegin;
                SendMessage(inkBegin);
            }

        }
    }
}
