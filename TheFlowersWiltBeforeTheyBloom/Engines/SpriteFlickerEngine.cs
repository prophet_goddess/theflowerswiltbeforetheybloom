﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Reads(typeof(SpriteComponent))]
    [Receives(typeof(SpriteFlickerMessage))]
    [Sends(typeof(UpdateSpriteMessage), typeof(DoneFlickeringMessage))]
    public class SpriteFlickerEngine : Engine
    {
        private Queue<(Entity, float, float, float)> flickering;

        public override void Update(double dt)
        {
            if(flickering == null)
            {
                flickering = new Queue<(Entity, float, float, float)>();
            }

            foreach(var flickerMessage in ReadMessages<SpriteFlickerMessage>())
            {
                if (!flickering.Any(s => s.Item1 == flickerMessage.spriteEntity))
                {
                    flickering.Enqueue((flickerMessage.spriteEntity, flickerMessage.duration, flickerMessage.speed, flickerMessage.speed));
                }

            }

            for (int i = 0; i < flickering.Count; i++)
            {
                var flicker = flickering.Dequeue();
                flicker.Item2 -= (float)dt;
                flicker.Item4 -= (float)dt;
                var sprite = GetComponent<SpriteComponent>(flicker.Item1);

                if (flicker.Item2 > 0f)
                {
                    if (flicker.Item4 <= 0f)
                    {
                        flicker.Item4 = flicker.Item3;
                        UpdateSpriteMessage updateSpriteMessage;
                        updateSpriteMessage.spriteEntity = flicker.Item1;
                        updateSpriteMessage.flicker = !sprite.flicker;
                        updateSpriteMessage.scale = Vector2.One;
                        SendMessage(updateSpriteMessage);
                    }
                    flickering.Enqueue(flicker);
                }
                else
                {
                    UpdateSpriteMessage updateSpriteMessage;
                    updateSpriteMessage.spriteEntity = flicker.Item1;
                    updateSpriteMessage.flicker = false;
                    updateSpriteMessage.scale = Vector2.One;
                    SendMessage(updateSpriteMessage);

                    DoneFlickeringMessage doneFlickeringMessage;
                    doneFlickeringMessage.entity = flicker.Item1;
                    SendMessage(doneFlickeringMessage);
                }
            }


        }
    }
}
