﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;
using Microsoft.Xna.Framework;
using System;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Receives(typeof(CreateEnemyMessage))]
    [Sends(typeof(CreateEncounterInfoMessage), typeof(CreateStatusMessage), typeof(CreateSpriteMessage), typeof(CreateTransformMessage))]
    [Writes(typeof(EnemyComponent))]
    [Reads(typeof(EnemyComponent))]
    public class EnemyEngine : Engine
    {
        public override void Update(double dt)
        {
            foreach (CreateEnemyMessage createEnemyMessage in ReadMessages<CreateEnemyMessage>()) {
                var enemyEntity = CreateEntity();

                CreateEncounterInfoMessage createEncounterInfoMessage;
                createEncounterInfoMessage.entity = enemyEntity;
                SendMessage(createEncounterInfoMessage);

                CreateStatusMessage createStatus;
                createStatus.entity = enemyEntity;
                createStatus.name = "Strange Phenomenon " + (createEnemyMessage.index + 1);

                createStatus.strength = RNG.GetInt(1, 5);
                createStatus.stamina = RNG.GetInt(1, 5);
                createStatus.speed = RNG.GetInt(1, 5);
                createStatus.special = RNG.GetInt(1, 5);

                SendMessage(createStatus);

                CreateSpriteMessage createSprite;
                createSprite.entity = enemyEntity;
                createSprite.scale = Vector2.One;
                createSprite.texture = Utility.GetEnemySprite(RNG.GetFloat(0f, 360f));
                createSprite.color = Color.White;
                createSprite.offset = Vector2.Zero;
                SendMessage(createSprite);

                CreateTransformMessage createTransform;
                createTransform.entity = enemyEntity;
                createTransform.position = createEnemyMessage.position;
                SendMessage(createTransform);

                SetComponent(enemyEntity, new EnemyComponent());

            }
        }
    }
}
