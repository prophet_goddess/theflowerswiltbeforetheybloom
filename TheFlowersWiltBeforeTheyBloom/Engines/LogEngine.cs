﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;
using System.Text;
using System.Linq;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Receives(typeof(AddToCombatLogMessage), typeof(ContinueMessage))]
    [Reads(typeof(LogComponent))]
    [Writes(typeof(LogComponent))]
    public class LogEngine : Engine
    {
        private int messageIndex = 0;
        private float timer = 0f;

        public override void Update(double dt)
        {
            var addToCombatLogMessages = ReadMessages<AddToCombatLogMessage>();
            (var combatLog, var combatLogEntity) = ReadComponentIncludingEntity<LogComponent>();

            foreach(var addToCombatLog in addToCombatLogMessages)
            {
                StringBuilder message = new StringBuilder();
                foreach (string word in addToCombatLog.message.Split(' '))
                {
                    var line = message.ToString().Split(System.Environment.NewLine).Last();                
                    if (Game.bodyFont.MeasureString(line).X >= Game.windowWidth - Game.boxBorderSize - Game.boxTextMargin)
                    {
                        message.Append(System.Environment.NewLine);
                    }
                    message.Append(" ");
                    message.Append(word);

                }
                combatLog.messages.Push((message.ToString(), addToCombatLog.characterDelay));
                combatLog.displaying = true;
                messageIndex = 0;
                SetComponent(combatLogEntity, combatLog);

                return;
            }

            if(combatLog.messages.Count == 0)
            {
                return;
            }

            (var displayMessage, var delay) = combatLog.messages.Peek();

            foreach(var message in ReadMessages<ContinueMessage>())
            {
                combatLog.displaying = false;
                combatLog.displayMessage = displayMessage;
                messageIndex = displayMessage.Length + 1;
            }

            if (combatLog.displaying && (timer >= delay || messageIndex == 0))
            {                
                combatLog.displayMessage = displayMessage.Substring(0, messageIndex);
                messageIndex++;
                timer = 0f;
            }

            if(messageIndex > displayMessage.Length)
            {
                combatLog.displaying = false;
            }

            SetComponent(combatLogEntity, combatLog);

            timer += (float)dt;
            
        }
    }
}
