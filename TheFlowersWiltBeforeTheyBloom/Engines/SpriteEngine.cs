﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;
using System.Collections.Generic;
using System.Linq;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Reads(typeof(SpriteComponent))]
    [Writes(typeof(SpriteComponent))]
    [Receives(typeof(UpdateSpriteMessage), typeof(CreateSpriteMessage), typeof(RemoveSpriteMessage))]
    public class SpriteEngine : Engine
    {
        public override void Update(double dt)
        {
            var createSpriteMessages = ReadMessages<CreateSpriteMessage>();
            foreach(var createSprite in createSpriteMessages) {
                SpriteComponent sprite;
                sprite.color = createSprite.color;
                sprite.flicker = false;
                sprite.scale = createSprite.scale;
                sprite.texture = createSprite.texture;
                sprite.offset = createSprite.offset;
                SetComponent(createSprite.entity, sprite);
            }

            var removeSpriteMessages = ReadMessages<RemoveSpriteMessage>();
            foreach(var removeSprite in removeSpriteMessages) {
                RemoveComponent<SpriteComponent>(removeSprite.entity);
            }

            var updates = new Dictionary<Entity, UpdateSpriteMessage>();

            foreach(var updateSprite in ReadMessages<UpdateSpriteMessage>())
            {
                if (updates.ContainsKey(updateSprite.spriteEntity))
                {
                    var newUpdate = updateSprite;
                    var oldUpdate = updates[updateSprite.spriteEntity];
                    newUpdate.flicker = oldUpdate.flicker;
                    updates[updateSprite.spriteEntity] = newUpdate;
                }
                else
                {
                    updates[updateSprite.spriteEntity] = updateSprite;
                }

            }

            foreach(var update in updates)
            {
                SpriteComponent sprite = GetComponent<SpriteComponent>(update.Key);
                sprite.flicker = update.Value.flicker;
                sprite.scale = update.Value.scale;
                SetComponent(update.Key, sprite);
            }
        }
    }
}
