﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{

    [Reads(typeof(PartyComponent))]
    [Writes(typeof(PartyComponent))]
    [Sends(typeof(CreateSpriteMessage), typeof(CreateTransformMessage))]
    [Receives(typeof(CreatePartyMessage))]
    public class PartyEngine : Engine
    {
        public override void Update(double dt) {
            if (SomeMessage<CreatePartyMessage>() && !SomeComponent<PartyComponent>()) {
                var partyEntity = CreateEntity();
                PartyComponent partyComponent;


                SetComponent(partyEntity, partyComponent);
            }

            if (!SomeComponent<PartyComponent>()) {
                return;
            }

        }
    }
}
