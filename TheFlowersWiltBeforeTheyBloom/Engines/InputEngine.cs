﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Messages;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Sends(typeof(ContinueMessage), typeof(MoveSelectorMessage))]
    public class InputEngine : Engine
    {
        private List<Keys> heldKeys;

        public override void Update(double dt)
        {
            if(heldKeys == null)
            {
                heldKeys = new List<Keys>();
            }

            var kstate = Keyboard.GetState();

            if (WasPressed(kstate, Keys.Z))
            {
                SendMessage(new ContinueMessage());
            }

            else if(WasPressed(kstate, Keys.Down))
            {
                MoveSelectorMessage moveSelectorMessage;
                moveSelectorMessage.direction = Direction.Down;
                SendMessage(moveSelectorMessage);
            }
            
            else if(WasPressed(kstate, Keys.Up))
            {
                MoveSelectorMessage moveSelectorMessage;
                moveSelectorMessage.direction = Direction.Up;
                SendMessage(moveSelectorMessage);
            }


            else if (WasPressed(kstate, Keys.Left))
            {
                MoveSelectorMessage moveSelectorMessage;
                moveSelectorMessage.direction = Direction.Left;
                SendMessage(moveSelectorMessage);
            }

            else if (WasPressed(kstate, Keys.Right))
            {
                MoveSelectorMessage moveSelectorMessage;
                moveSelectorMessage.direction = Direction.Right;
                SendMessage(moveSelectorMessage);
            }


            foreach (var key in kstate.GetPressedKeys())
            {
                if(heldKeys.IndexOf(key) == -1)
                {
                    heldKeys.Add(key);
                }
            }

            heldKeys.RemoveAll(k => kstate.IsKeyUp(k));

        }

        private bool WasPressed(KeyboardState state, Keys key)
        {
            return state.IsKeyDown(key) && heldKeys.IndexOf(key) == -1;
        }
    }
}
