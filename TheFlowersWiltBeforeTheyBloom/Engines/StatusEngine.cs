﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;
using Microsoft.Xna.Framework;
using System;
using System.Diagnostics;

namespace TheFlowersWiltBeforeTheyBloom.Engines {
    [Reads(typeof(StatusComponent), typeof(EnemyComponent), typeof(PartyMemberComponent), typeof(TransformComponent), typeof(SpriteComponent))]
    [Receives(typeof(HPChangeMessage), typeof(SPChangeMessage), typeof(StatChangeMessage), typeof(CreateStatusMessage))]
    [Writes(typeof(StatusComponent))]
    [Sends(typeof(AddToCombatLogMessage), typeof(ShakeMessage), typeof(SpriteFlickerMessage), typeof(KillMessage))]
    public class StatusEngine : Engine {
        public override void Update(double dt) {
            var hpChangeMessages = ReadMessages<HPChangeMessage>();
            var spChangeMessages = ReadMessages<SPChangeMessage>();
            var createStatusMessages = ReadMessages<CreateStatusMessage>();


            foreach (var createStatusMessage in createStatusMessages) {
                StatusComponent status;
                status.name = createStatusMessage.name;
                status.special = createStatusMessage.special;
                status.speed = createStatusMessage.speed;
                status.stamina = createStatusMessage.stamina;
                status.strength = createStatusMessage.strength;

                status.hitPoints = status.maxHitPoints = Utility.GetMaxHP(status.stamina);
                status.specialPoints = status.maxSpecialPoints = Utility.GetMaxSP(status.special);
                SetComponent(createStatusMessage.entity, status);

            }

            foreach (var hpChangeMessage in hpChangeMessages) {
                var target = GetComponent<StatusComponent>(hpChangeMessage.targetEntity);
                target.hitPoints += hpChangeMessage.amount;
                SetComponent(hpChangeMessage.targetEntity, target);

                if (hpChangeMessage.amount < 0) {
                    AddToCombatLogMessage addToCombatLogMessage;
                    addToCombatLogMessage.characterDelay = 0.025f;
                    addToCombatLogMessage.message = Utility.GetLineWithId("damageDealt")
                                                    .Replace("%amount", Math.Abs(hpChangeMessage.amount).ToString())
                                                    .Replace("%attacker", hpChangeMessage.sourceName)
                                                    .Replace("%target", target.name);

                    SendMessage(addToCombatLogMessage);

                    ShakeMessage shakeMessage;
                    shakeMessage.transformEntity = hpChangeMessage.targetEntity;
                    shakeMessage.intensity = 5f;
                    shakeMessage.duration = 0.25f;
                    SendMessage(shakeMessage);

                    if (HasComponent<EnemyComponent>(hpChangeMessage.targetEntity)) {
                        SpriteFlickerMessage spriteFlickerMessage;
                        spriteFlickerMessage.spriteEntity = hpChangeMessage.targetEntity;
                        spriteFlickerMessage.speed = 0.015f;
                        spriteFlickerMessage.duration = 0.25f;
                        SendMessage(spriteFlickerMessage);

                    }

                    if (target.hitPoints <= 0) {
                        KillMessage killMessage;
                        killMessage.statusEntity = hpChangeMessage.targetEntity;
                        SendMessage(killMessage);
                    }
                }
                else if (hpChangeMessage.amount > 0) {
                    AddToCombatLogMessage addToCombatLogMessage;
                    addToCombatLogMessage.characterDelay = 0.025f;
                    addToCombatLogMessage.message = Utility.GetLineWithId("damageHealed")
                                                   .Replace("%amount", hpChangeMessage.amount.ToString())
                                                   .Replace("%healer", hpChangeMessage.sourceName)
                                                   .Replace("%target", target.name);
                    SendMessage(addToCombatLogMessage);
                }

            }

            foreach (var spChangeMessage in spChangeMessages) {
                var target = GetComponent<StatusComponent>(spChangeMessage.targetEntity);
                target.specialPoints += spChangeMessage.amount;
                SetComponent(spChangeMessage.targetEntity, target);
            }
        }
    }
}
