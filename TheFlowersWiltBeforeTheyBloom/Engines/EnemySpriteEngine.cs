﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;
using Microsoft.Xna.Framework;
using System;
using System.Linq;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Reads(typeof(EnemyComponent), typeof(SpriteComponent), typeof(StatusComponent), typeof(EncounterInfoComponent), typeof(TransformComponent))]
    [Sends(typeof(UpdateTransformMessage))]
    public class EnemySpriteEngine : Engine
    {
        private double time = 0f;

        public override void Update(double dt)
        {
            time += dt;

            var enemies = ReadComponentsIncludingEntity<EnemyComponent>().ToArray();
            var enemyCount = enemies.Length;

            var i = enemyCount % 2 == 0 ? 1 : 0;
            var j = i;
            foreach ((var enemy, var enemyEntity) in enemies)
            {
                float t = (float)Math.Sin((Game.gameTime.TotalGameTime.TotalSeconds + (enemy.GetHashCode() % MathHelper.TwoPi))) * 8f;

                var sprite = GetComponent<SpriteComponent>(enemyEntity);
                var transform = GetComponent<TransformComponent>(enemyEntity);

                var spriteMargin = (enemyCount % 2 == 0 ? 0.75f : 1.33f);
                var xOffset = (sprite.texture.Width * spriteMargin) * j * (i % 2 == 0 ? 1 : -1);

                UpdateTransformMessage updateTransformMessage;
                updateTransformMessage.transformEntity = enemyEntity;
                updateTransformMessage.newPosition = new Vector2((Game.windowWidth / 2) + xOffset, (Game.windowHeight / 2) + t);
                SendMessage(updateTransformMessage);

                i++;
                if (i % 2 != 0)
                {
                    j += (enemyCount % 2 == 0 ? 2 : 1);
                }
            }


        }
    }
}
