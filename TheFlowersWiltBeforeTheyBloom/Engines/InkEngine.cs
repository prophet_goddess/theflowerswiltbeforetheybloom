﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;
using Ink.Runtime;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TheFlowersWiltBeforeTheyBloom.Engines {
    [Receives(typeof(ContinueMessage), typeof(InitializeInkMessage))]
    [Reads(typeof(InkStoryComponent), typeof(PartyMemberComponent), typeof(ChoicesComponent), typeof(EncounterComponent), typeof(StatusComponent), typeof(LogComponent))]
    [Writes(typeof(InkStoryComponent))]
    [Sends(typeof(AddToCombatLogMessage), typeof(InkCommandMessage), typeof(CreateChoiceMessage))]
    public class InkEngine : Engine {
        private string ParseLine(string line) {
            var getPartyMembers = Regex.Matches(line, @"\%partyMember\:\d");
            foreach (var getPartyMember in getPartyMembers) {
                var substr = getPartyMember.ToString();
                var partyMembers = ReadComponentsIncludingEntity<PartyMemberComponent>().ToArray();
                var partyMemberIndex = System.Convert.ToInt32(substr.Substring(substr.Length - 1, 1));
                (var partyMember, var partyMemberEntity) = partyMembers[partyMemberIndex];
                var partyMemberStatus = GetComponent<StatusComponent>(partyMemberEntity);

                line = line.Replace(substr, partyMemberStatus.name);
            }

            return line;
        }

        private void Continue(Story story) {
            if (story.canContinue) {
                var nextLine = story.Continue();

                if (!story.currentText.StartsWith("$")) {
                    AddToCombatLogMessage sendNextLine;
                    sendNextLine.message = ParseLine(nextLine);
                    sendNextLine.characterDelay = 0.025f;
                    SendMessage(sendNextLine);
                }
            }
            else {
                if (story.currentChoices.Count > 0 && !SomeComponent<ChoicesComponent>()) {
                    CreateChoiceMessage createChoice;
                    createChoice.choices = story.currentChoices.Select(c => c.text).ToArray();
                    SendMessage(createChoice);
                }
            }
        }

        public override void Update(double dt) {
            if (SomeComponent<EncounterComponent>()) {
                return;
            }

            (var inkStoryComponent, var inkStoryEntity) = ReadComponentIncludingEntity<InkStoryComponent>();

            //continue the frame after running the command, so that the text parser has any information created by commands the previous frame.
            //so for instance if you use $addPartyMember and then in the next line Deferredly reference the new party member's name, it won't error
            if (inkStoryComponent.story.currentText.StartsWith("$")) {
                Continue(inkStoryComponent.story);
            }

            if (SomeComponent<ChoicesComponent>()) {
                (var choices, var choicesEntity) = ReadComponentIncludingEntity<ChoicesComponent>();
                if (choices.selected && inkStoryComponent.story.currentChoices.Count > 0) {
                    inkStoryComponent.story.ChooseChoiceIndex(choices.selectedIndex);
                    Continue(inkStoryComponent.story);
                    Destroy(choicesEntity);
                }
                else {
                    return;
                }
            }

            if (SomeMessage<ContinueMessage>()) {
                var combatLog = ReadComponent<LogComponent>();
                if (!combatLog.displaying) {
                    Continue(inkStoryComponent.story);
                }
            }

            if (inkStoryComponent.story.currentText.StartsWith("$")) {
                InkCommandMessage inkCommandMessage;
                inkCommandMessage.command = inkStoryComponent.story.currentText;
                SendMessage(inkCommandMessage);
            }

            SetComponent(inkStoryEntity, inkStoryComponent);
        }
    }
}
