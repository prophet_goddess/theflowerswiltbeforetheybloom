﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Messages;
using TheFlowersWiltBeforeTheyBloom.Components;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Reads(typeof(TransformComponent))]
    [Receives(typeof(ShakeMessage))]
    [Sends(typeof(UpdateTransformMessage), typeof(DoneShakingMessage))]
    public class ShakerEngine : Engine
    {
        private Queue<(Entity, Vector2, float, float)> shaking;

        public override void Update(double dt)
        {
            if(shaking == null)
            {
                shaking = new Queue<(Entity, Vector2, float, float)>();
            }

            foreach(var shakeMessage in ReadMessages<ShakeMessage>())
            {
                if(!shaking.Any(s => s.Item1 == shakeMessage.transformEntity))
                {
                    var transform = GetComponent<TransformComponent>(shakeMessage.transformEntity);
                    shaking.Enqueue((shakeMessage.transformEntity, transform.position, shakeMessage.duration, shakeMessage.intensity));
                }
            }

            for(int i = 0; i < shaking.Count; i++)
            {
                var shake = shaking.Dequeue();
                shake.Item3 -= (float)dt;

                if(shake.Item3 >= 0f)
                {
                    UpdateTransformMessage updateTransformMessage;
                    updateTransformMessage.transformEntity = shake.Item1;
                    updateTransformMessage.newPosition = shake.Item2 + (new Vector2(RNG.GetFloat(), RNG.GetFloat()) * shake.Item4);
                    SendMessage(updateTransformMessage);
                    shaking.Enqueue(shake);
                }
                else
                {
                    UpdateTransformMessage updateTransformMessage;
                    updateTransformMessage.transformEntity = shake.Item1;
                    updateTransformMessage.newPosition = shake.Item2;
                    SendMessage(updateTransformMessage);

                    DoneShakingMessage doneShakingMessage;
                    doneShakingMessage.entity = shake.Item1;
                    SendMessage(doneShakingMessage);
                }
            }
        }
    }
}
