﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;
using Microsoft.Xna.Framework;
using System;
using System.Linq;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Sends(typeof(CreateEnemyMessage), typeof(AddToCombatLogMessage))]
    [Receives(typeof(CreateEncounterMessage))]
    [Writes(typeof(EncounterComponent))]
    public class CreateEncounterEngine : Engine
    {
        private int minEnemies = 1;
        private int maxEnemies = 5;

        public override void Update(double dt)
        {
            var encounterMessages = ReadMessages<CreateEncounterMessage>();
            foreach (var encounterMessage in encounterMessages)
            {
                var encounterEntity = CreateEntity();
                SetComponent(encounterEntity, new EncounterComponent());

                int numEnemies = RNG.GetIntInclusive(minEnemies, maxEnemies);


                for (int i = 0, j = 0; i < numEnemies; i++)
                {
                    var spriteMargin = (numEnemies % 2 == 0 ? 0.75f : 1.33f);
                    var xOffset = (Utility.enemySpriteWidth * spriteMargin) * j * (i % 2 == 0 ? 1 : -1);

                    CreateEnemyMessage createEnemyMessage;
                    createEnemyMessage.index = i;
                    createEnemyMessage.position = new Vector2((Game.windowWidth / 2) + xOffset, (Game.windowHeight / 2));
                    SendMessage(createEnemyMessage);

                    if (i % 2 != 0)
                    {
                        j += (numEnemies % 2 == 0 ? 2 : 1);
                    }
                }

                AddToCombatLogMessage addToCombatLogMessage;
                addToCombatLogMessage.characterDelay = 0.025f;
                addToCombatLogMessage.message = Utility.GetLineWithId("encounterStart").Replace("%numEnemies", "" + numEnemies);

                SendMessage(addToCombatLogMessage);
            }
        }
    }
}
