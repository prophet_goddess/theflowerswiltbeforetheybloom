﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Messages;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Reads(typeof(SpriteComponent), typeof(StatusComponent), typeof(EnemyComponent), typeof(EncounterInfoComponent))]
    [Receives(typeof(KillMessage), typeof(DoneFlickeringMessage), typeof(DoneShakingMessage))]
    [Sends(typeof(UpdateSpriteMessage))]
    public class KillEngine : Engine
    {
        //bool 1: done flickering, bool 2: done shaking
        public Dictionary<Entity, (bool, bool)> killList;

        public override void Update(double dt)
        {

            if(killList == null)
            {
                killList = new Dictionary<Entity, (bool, bool)>();
            }

            foreach(var message in ReadMessages<KillMessage>())
            {
                RemoveComponent<StatusComponent>(message.statusEntity);
                RemoveComponent<EncounterInfoComponent>(message.statusEntity);

                killList.Add(message.statusEntity, (false, false));
            }

            foreach(var message in ReadMessages<DoneFlickeringMessage>())
            {
                if(killList.ContainsKey(message.entity))
                {
                    killList[message.entity] = (true, killList[message.entity].Item2);   
                }
            }

            foreach(var message in ReadMessages<DoneShakingMessage>())
            {
                if (killList.ContainsKey(message.entity))
                {
                    killList[message.entity] = (killList[message.entity].Item1, true);
                }
            }

            var toRemove = new List<Entity>();
            foreach(var kill in killList)
            {
                if(kill.Value.Item1 && kill.Value.Item2)
                {
                    Destroy(kill.Key);
                    toRemove.Add(kill.Key);
                }
            }

            foreach(var key in toRemove)
            {
                killList.Remove(key);
            }
        }
    }
}
