﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;

namespace TheFlowersWiltBeforeTheyBloom.Engines
{
    [Reads(typeof(BackgroundComponent))]
    [Writes(typeof(BackgroundComponent))]
    public class BackgroundEngine : Engine
    {
        public override void Update(double dt)
        {
            if (!SomeComponent<BackgroundComponent>())
            {
                return;
            }

            var (backgroundComponent, entity) = ReadComponentIncludingEntity<BackgroundComponent>();

            foreach (var parameter in backgroundComponent.effect.Parameters)
            {
                if (parameter.Name == "time")
                {
                    parameter.SetValue(parameter.GetValueSingle() + (float)dt);
                }
            }

            SetComponent(entity, backgroundComponent);
        }
    }
}
