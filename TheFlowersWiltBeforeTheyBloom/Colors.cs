﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheFlowersWiltBeforeTheyBloom
{
    class Colors
    {
        public static Color white = new Color(255, 255, 255, 255);
        public static Color black = new Color(0, 0, 0, 255);
        public static Color outline = new Color(38, 43, 68, 255);
    }
}
