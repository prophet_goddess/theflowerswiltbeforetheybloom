#define SV_POSITION POSITION
#define VS_SHADERMODEL vs_3_0
#define PS_SHADERMODEL ps_3_0

uniform float time;
Texture2D SpriteTexture;

sampler2D SpriteTextureSampler = sampler_state {
	Texture = <SpriteTexture>;
};

struct VertexShaderOutput {
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
};

float rand(float x){
	return frac(sin(x) * 10000000.0);
}

float4 MainPS(VertexShaderOutput input) : COLOR
{
	float3 rgb = tex2D(SpriteTextureSampler, input.TextureCoordinates);

	float i = floor((time) * 2.0);
	float f = frac((time) * 2.0);

	float u = f * f * (3.0 - 2.0 * f);
	float levels = 8 + ((lerp(rand(i), rand(i + 1.0), u)));

	rgb *= levels;
	rgb = round(rgb);
	rgb /= levels;
	return float4(rgb, 1.0);

}

technique SpriteDrawing {
	pass P0 {
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};