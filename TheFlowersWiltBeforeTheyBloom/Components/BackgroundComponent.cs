﻿using Encompass;
using Microsoft.Xna.Framework.Graphics;

namespace TheFlowersWiltBeforeTheyBloom.Components
{
    public struct BackgroundComponent : IComponent
    {
        public Texture2D texture;
        public Effect effect;
    }
}
