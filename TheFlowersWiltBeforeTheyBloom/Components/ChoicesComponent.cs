﻿using Encompass;
using Ink.Runtime;
using System.Collections.Generic;

namespace TheFlowersWiltBeforeTheyBloom.Components
{
    public struct ChoicesComponent : IComponent
    {
        public string[] choices;
        public int selectedIndex;
        public bool selected;
    }
}
