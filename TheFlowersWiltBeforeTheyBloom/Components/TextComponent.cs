﻿using Encompass;
using Microsoft.Xna.Framework;

namespace TheFlowersWiltBeforeTheyBloom.Components
{
    public struct TextComponent : IComponent
    {
        public string text;
        public Vector2 offset;
    }
}
