﻿using Encompass;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace TheFlowersWiltBeforeTheyBloom.Components
{
    public struct LogComponent : IComponent
    {
        public Stack<(string, float)> messages;
        public bool displaying;
        public string displayMessage;
        public float relativeHeight;
    }
}
