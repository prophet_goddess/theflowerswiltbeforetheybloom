﻿using Encompass;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TheFlowersWiltBeforeTheyBloom.Components
{
    public struct SpriteComponent : IComponent
    {
        public Texture2D texture;
        public Color color;
        public bool flicker;
        public Vector2 scale;
        public Vector2 offset;
    }
}
