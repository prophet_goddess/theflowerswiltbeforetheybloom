﻿using Encompass;

namespace TheFlowersWiltBeforeTheyBloom.Components
{
    public struct EncounterInfoComponent : IComponent
    {
        public bool targeted;
        public TurnState turnState;
    }
}
