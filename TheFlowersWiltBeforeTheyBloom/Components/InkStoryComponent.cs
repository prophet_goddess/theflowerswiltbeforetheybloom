﻿using Encompass;
using Ink.Runtime;

namespace TheFlowersWiltBeforeTheyBloom.Components
{
    public struct InkStoryComponent : IComponent
    {
        public Story story;
    }
}
