﻿using Encompass;

namespace TheFlowersWiltBeforeTheyBloom.Components
{
    public enum TurnState {
        NotTakenTurn, IsTurn, TakenTurn
    }

    public struct StatusComponent : IComponent
    {
        public string name;

        public int strength; //affects all attack damage
        public int speed; //affects turn order
        public int special; //affects mana
        public int stamina; //affects health

        public int maxHitPoints;
        public int hitPoints;
        public int maxSpecialPoints;
        public int specialPoints;
    }
}
