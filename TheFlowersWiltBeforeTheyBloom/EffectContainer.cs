﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;
using TheFlowersWiltBeforeTheyBloom.IO;
using System.IO;

namespace TheFlowersWiltBeforeTheyBloom.Containers
{
    public sealed class EffectContainer
    {
        public Effect backgroundEffect { get; }
        public Effect posterizeEffect { get; }

        public EffectContainer(GraphicsDevice graphicsDevice)
        {
            backgroundEffect = EffectLoader.LoadEffect(graphicsDevice, Path.Combine(Game.instance.Content.RootDirectory, "Background.fxo"));
            posterizeEffect = EffectLoader.LoadEffect(graphicsDevice, Path.Combine(Game.instance.Content.RootDirectory, "Posterize.fxo"));
        }
    }
}
