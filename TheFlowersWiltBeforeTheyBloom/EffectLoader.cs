﻿using Microsoft.Xna.Framework.Graphics;
using System.IO;

namespace TheFlowersWiltBeforeTheyBloom.IO
{
    public static class EffectLoader
    {
        public static Effect LoadEffect(GraphicsDevice graphicsDevice, string path)
        {
            return new Effect(graphicsDevice, File.ReadAllBytes(path));
        }
    }
}