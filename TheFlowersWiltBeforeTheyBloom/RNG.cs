﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace TheFlowersWiltBeforeTheyBloom
{
    public static class RNG
    {
        private static Random random;

        public static void Initialize()
        {
            random = new Random();
        }

        public static float GetFloat(float min = 0f, float max = 1f)
        {
            return (float)random.NextDouble() * (max - min) + min;
        }

        public static int GetInt(int min, int max)
        {
            return (int)Math.Floor(GetFloat(min, max));
        }

        public static int GetIntInclusive(int min, int max)
        {
            return GetInt(min, max + 1);
        }

        public static T GetRandomItem<T>(this T[] array)
        {
            return array[GetInt(0, array.Length)];
        }

        public static XmlNode GetRandomNode(this XmlNodeList nodes)
        {
            return nodes[GetInt(0, nodes.Count)];
        }
    }
}
