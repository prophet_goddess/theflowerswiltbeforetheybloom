﻿using Encompass;
using TheFlowersWiltBeforeTheyBloom.Components;
using TheFlowersWiltBeforeTheyBloom.Engines;
using TheFlowersWiltBeforeTheyBloom.Messages;
using TheFlowersWiltBeforeTheyBloom.Renderers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.Diagnostics;
using Ink.Runtime;
using System.Xml;
using System.IO;
using SpriteFontPlus;
using TheFlowersWiltBeforeTheyBloom.Containers;

namespace TheFlowersWiltBeforeTheyBloom
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game : Microsoft.Xna.Framework.Game
    {
        public static Game instance;

        public static int windowWidth = 640*2;
        public static int windowHeight = 480*2;

        public static int boxBorderSize = 8;
        public static int boxTextMargin = 12;

        public static GameTime gameTime;

        public static Story story;
        public static string inkAsset;
        public static XmlDocument encounterText;

        public static GraphicsDeviceManager graphics;
        public static SpriteBatch spriteBatch;

        public static Texture2D testBackground;
        public static Texture2D white16;
        public static Effect paletteEffect;
        public static Texture2D selectorSprite;
        public static SpriteFont bodyFont;
        public static Dictionary<string, Texture2D> tarotCardSprites;

        public static EffectContainer effectContainer;

        World world;

        private RenderTarget2D renderTarget;

        public Game()
        {
            instance = this;

            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = windowWidth;
            graphics.PreferredBackBufferHeight = windowHeight;
            graphics.PreferMultiSampling = false;

            Window.AllowUserResizing = true;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            RNG.Initialize();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {

            Window.Title = "the flowers wilt before they bloom";

            effectContainer = new EffectContainer(GraphicsDevice);


            inkAsset = File.ReadAllText(Path.Combine(Content.RootDirectory, "story.json"));
            encounterText = new XmlDocument();
            encounterText.Load(Path.Combine(Content.RootDirectory, "EncounterText.xml"));

            renderTarget = new RenderTarget2D(graphics.GraphicsDevice, windowWidth, windowHeight, false, SurfaceFormat.Color, DepthFormat.None, 0, RenderTargetUsage.PlatformContents); ;

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            bodyFont = TtfFontBaker.Bake(File.ReadAllBytes(Path.Combine(Content.RootDirectory, "Silver.ttf")), 64, 1024, 1024, 
                new[] { CharacterRange.BasicLatin, CharacterRange.LatinExtendedA, CharacterRange.LatinExtendedB, CharacterRange.Latin1Supplement})
                .CreateSpriteFont(GraphicsDevice);
            using (FileStream fileStream = File.OpenRead(Path.Combine(Content.RootDirectory, "box.png")))
            {
                white16 = Texture2D.FromStream(GraphicsDevice, fileStream);
            }
            using(FileStream fileStream = File.OpenRead(Path.Combine(Content.RootDirectory, "selector.png")))
            {
                selectorSprite = Texture2D.FromStream(GraphicsDevice, fileStream);
            }
            using(FileStream fileStream = File.OpenRead(Path.Combine(Content.RootDirectory, "bedroom.png")))
            {
                testBackground = Texture2D.FromStream(GraphicsDevice, fileStream);
            }

            WorldBuilder worldBuilder = new WorldBuilder();

            worldBuilder.AddEngine(new BackgroundEngine());
            worldBuilder.AddEngine(new LogEngine());
            worldBuilder.AddEngine(new InkEngine());
            worldBuilder.AddEngine(new InitializeInkEngine());
            worldBuilder.AddEngine(new InkCommandEngine());
            worldBuilder.AddEngine(new InputEngine());
            worldBuilder.AddEngine(new StatusEngine());
            worldBuilder.AddEngine(new EnemySpriteEngine());
            worldBuilder.AddEngine(new SpriteEngine());
            worldBuilder.AddEngine(new TransformEngine());
            worldBuilder.AddEngine(new ShakerEngine());
            worldBuilder.AddEngine(new SpriteFlickerEngine());
            worldBuilder.AddEngine(new EncounterEngine());
            worldBuilder.AddEngine(new CreateEncounterEngine());
            worldBuilder.AddEngine(new PartyMemberEngine());
            worldBuilder.AddEngine(new EnemyEngine());
            worldBuilder.AddEngine(new KillEngine());
            worldBuilder.AddEngine(new ChoiceEngine());
            worldBuilder.AddEngine(new PartyEngine());
            worldBuilder.AddEngine(new TextEngine());

            worldBuilder.AddGeneralRenderer(new BackgroundRenderer(), 0);

            worldBuilder.AddGeneralRenderer(new SpriteRenderer(), 1);
            worldBuilder.AddGeneralRenderer(new EnemyHealthRenderer(), 1);
            worldBuilder.AddGeneralRenderer(new TargetSelectRenderer(), 1);
            worldBuilder.AddGeneralRenderer(new LogRenderer(), 1);
            worldBuilder.AddGeneralRenderer(new SidebarRenderer(), 1);
            worldBuilder.AddGeneralRenderer(new ChoiceRenderer(), 1);
            worldBuilder.AddGeneralRenderer(new TextRenderer(), 2);


            Entity inkStoryEntity = worldBuilder.CreateEntity();
            InkStoryComponent inkStoryComponent;
            inkStoryComponent.story = new Story(inkAsset);
            worldBuilder.SetComponent(inkStoryEntity, inkStoryComponent);

            Entity logEntity = worldBuilder.CreateEntity();

            LogComponent logComponent;
            logComponent.messages = new Stack<(string, float)>();
            logComponent.displayMessage = "";
            logComponent.relativeHeight = 1f / 3f;
            logComponent.displaying = false;
            worldBuilder.SetComponent(logEntity, logComponent);

            Entity sidebarEntity = worldBuilder.CreateEntity();

            SidebarComponent sidebarComponent;
            worldBuilder.SetComponent(sidebarEntity, sidebarComponent);

            Entity backgroundEntity = worldBuilder.CreateEntity();
            BackgroundComponent background;
            background.texture = testBackground;
            background.effect = effectContainer.posterizeEffect;

            worldBuilder.SetComponent(backgroundEntity, background);

            InitializeInkMessage initializeInkMessage;
            worldBuilder.SendMessage(initializeInkMessage);

            world = worldBuilder.Build();
        }

        protected override void Update(GameTime gameTime)
        {
            Game.gameTime = gameTime;
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            world.Update(gameTime.ElapsedGameTime.TotalSeconds);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            var width = (int)(Window.ClientBounds.Height * ((float)windowWidth / windowHeight));
            var height = (int)(width * ((float)windowHeight / windowWidth));

            width = ((width + windowWidth / 2) / windowWidth) * windowWidth;
            height = ((height + windowHeight / 2) / windowHeight) * windowHeight;

            GraphicsDevice.SetRenderTarget(renderTarget);
            GraphicsDevice.Clear(Color.Green);

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.Default, RasterizerState.CullNone);
            world.Draw();
            spriteBatch.End();

            GraphicsDevice.SetRenderTarget(null);

            spriteBatch.Begin();
            spriteBatch.Draw(white16, new Rectangle(0, 0, Window.ClientBounds.Width, Window.ClientBounds.Height), Color.Black);
            spriteBatch.End();

            var gapSize = Window.ClientBounds.Height - height;

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.Opaque, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone);
            spriteBatch.Draw(renderTarget, new Rectangle((int)((Window.ClientBounds.Width - width) / 2f), gapSize/2, width, height), Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }

    }
}
